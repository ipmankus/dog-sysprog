import RPi.GPIO as GPIO
import subprocess
GPIO.setmode(GPIO.BCM)
GPIO.setup(15, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

proc = subprocess.Popen(['python', 'test2.py'])
pid = proc.pid


while True: 
    if GPIO.input(15) == GPIO.HIGH:
        subprocess.Popen(['kill', '-SIGINT', str(pid)])
	break
